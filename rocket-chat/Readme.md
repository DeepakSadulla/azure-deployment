# Rocket-Chat

## How to install
1. Visit the [Official Documentation](https://docs.rocket.chat/installation/docker-containers/docker-compose) on this
2. Run the following commands
```
cd ~/azure-deployment/rocket-chat/
mkdir data
curl -L https://raw.githubusercontent.com/RocketChat/Rocket.Chat/develop/docker-compose.yml -o docker-compose.yml
```
3. Open the docker-compose.yml file (using `nano docker-compose.yml`) and change `- ROOT_URL=http://localhost:3000` to `- ROOT_URL=0.0.0.0:3000`. [What is the difference between localhost, 127.0.0.1 and 0.0.0.0?](https://stackoverflow.com/questions/20778771/what-is-the-difference-between-0-0-0-0-127-0-0-1-and-localhost)
4. Go to Azure VM page, within Networking open port number 3000
5. Run the following command `sudo docker-compose up -d`
6. Visit IP:3000 and complete the administrator setup
7. Ask users to signup and use the messaging app
8. To bring the server down (for update, maintenance or other reasons), use `sudo docker-compose down` from the `~/azure-deployment/rocket-chat/` folder
