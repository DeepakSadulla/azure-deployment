Run the below commands :

```bash
git clone https://gitlab.com/DeepakSadulla/azure-deployment.git
cd azure-deployment/dash-visualization/
sudo docker-compose build
sudo docker-compose up -d
```

Detached Mode Flag `-d` used here to run the docker in a detached mode.

Go to Azure portal and open port 8050 within the Networking Section.. 

To close the docker, please run the below command
`sudo docker-compose down`