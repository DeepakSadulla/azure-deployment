# How to install a PostgreSQL Server

## [How to Install Docker](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository)
```bash
cd ~
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
docker --version
```

Add users to the docker user group so that they can use docker without sudo privileges: `sudo usermod -aG docker $USER`

## [How to Install Docker Compose](https://docs.docker.com/compose/install/#install-compose-on-linux-systems)
```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
docker-compose --version
```


## [How to run the Postgres Instance](https://hub.docker.com/_/postgres)
```bash
git clone https://gitlab.com/DeepakSadulla/azure-deployment.git
mkdir -p ~/azure-deployment/postgresql-server/docker/volumes/postgres
cd azure-deployment/postgresql-server/
sudo docker pull postgres
sudo docker run --rm --name postgres_server -e POSTGRES_PASSWORD=postgres -d -p 5432:5432 -v $HOME/azure-deployment/postgresql-server/docker/volumes/postgres:/var/lib/postgresql/data  postgres
sudo docker ps
sudo docker kill <CONTAINER_ID>
```

Please pay attention to the port flag 
1. Port Flag `-p` -- used to specify the port you would like to deploy on,
2. Folder/Volume Flag `-v` -- used to persist data across different multiple starts and stops of the docker instance, so that we dont remove data or configurations when we start or stop the docker container
3. Environment Flag `-e`  -- Setting up environment variables within the container,
4. Detached Mode Flag `-d` --  to run the docker in a detached mode

## Create data objects

Open SQL Editor in PgAdmin4 and create the connection using Azure VM's IP and Port 5432

Some sample queries to run and test quickly:

```sql
CREATE ROLE new_user WITH
	LOGIN
	SUPERUSER
	CREATEDB
	CREATEROLE
	INHERIT
	NOREPLICATION
	CONNECTION LIMIT -1
	PASSWORD 'qwerty321';

CREATE DATABASE "companies"
    WITH 
    OWNER = new_user
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;

CREATE SCHEMA data
    AUTHORIZATION new_user;

CREATE TABLE data.link (
	ID serial PRIMARY KEY,
	url VARCHAR (255) NOT NULL,
	name VARCHAR (255) NOT NULL,
	description VARCHAR (255),
	rel VARCHAR (50)
);

INSERT INTO data.link (url, name)
VALUES
 ('http://www.google.com','Google'),
 ('http://www.yahoo.com','Yahoo'),
 ('http://www.bing.com','Bing'),
 ('http://www.oreilly.com','O''Reilly Media'),
 ('https://www.postgresqltutorial.com','PostgreSQL Tutorial');

ALTER TABLE data.link ADD COLUMN last_update DATE;

ALTER TABLE data.link ALTER COLUMN last_update
SET DEFAULT CURRENT_DATE;

INSERT INTO data.link (url, name, last_update)
VALUES
	('http://www.facebook.com','Facebook','2013-06-01');

INSERT INTO data.link (url, name, last_update)
VALUES
	('https://www.tumblr.com/','Tumblr',DEFAULT);

```