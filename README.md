# Azure Deployment

## Objective
The objective of the webinar is to create awareness about cloud offerings (especially, VMs) within the organization and empower our colleagues with the right set of tools to create, collaborate, iterate and disseminate their applications for the most impact. We want you to think about how would one deploy your application so that users can extract the most out of it : )

## Pre-requisites
Here are some things you might need:
1. [Anaconda](https://docs.anaconda.com/anaconda/install/windows/) or [SSH](https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse) or [Putty](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)
2. [Azure Account](https://portal.azure.com/)
3. [Git](https://git-scm.com/download/win)
4. [PgAdmin4](https://www.pgadmin.org/download/pgadmin-4-windows/)

## Contents
This tutorial covers deployment of various open source softwares on Azure, namely:
1. [Jupyter Hub (The Littlest Jupyter Hub - TLJH version)](https://gitlab.com/DeepakSadulla/azure-deployment/-/tree/master/the-littlest-jupyter-hub)
2. [PostgreSQL Server](https://gitlab.com/DeepakSadulla/azure-deployment/-/tree/master/postgresql-server)
3. [Visualization in Dash](https://gitlab.com/DeepakSadulla/azure-deployment/-/tree/master/dash-visualization)
4. [Rocket Chat](https://gitlab.com/DeepakSadulla/azure-deployment/-/tree/master/rocket-chat)

The process should be very similar for other apps like Django applications, Shiny applications, Flask applications, Streamlit applications, other database servers, etc. The magical formula is to find (or create) the Dockerfile (or Docker Compose) of the application you want to deploy and follow similar steps as documented in these usecases. The process would also be very similar across various cloud providers as well, namely, Amazon Web Services, Google Cloud Platform, IBM Cloud etc.

## What to do next

### Learn more about Ubuntu (all of these topics are just a Google search away):
1. [Users, Groups, Permissions](https://www.linode.com/docs/tools-reference/linux-users-and-groups/)
2. Memory, CPU Usage `htop` & Disk `df`
3. Processes Running using `ps aux | grep <keyword>`, `htop` and killing Processes `kill -9 <PROCESS ID>`
4. [Standard Input, Standard Output, Standard Error](http://www.learnlinux.org.za/courses/build/shell-scripting/ch01s04.html)
5. Ports and Process IDs `lsof -i:<PORT NUMBER>`
6. [Bringing processes to (and from) Background & Foreground (`bg` `fg` `&` `nohup`)](https://www.thegeekdiary.com/understanding-the-job-control-commands-in-linux-bg-fg-and-ctrlz/)
7. Anaconda and VirtualEnv Environments
8. [File and Folder Permissions](https://www.linode.com/docs/tools-reference/linux-users-and-groups/)
9. [Shell or Bash Scripting Fundamentals](https://www.youtube.com/watch?v=wsh64rjnRas&t=8458s)

### How do you scale up a Dockerized App:
1. Parallelizing your app to use all cores and RAM
2. Scaling up the VMs in terms of cores and RAM
3. Using a Distributed Cluster with Kubernetes setup for container orchestration

### Here are some cool open source applications you could try to deploy:
1. [RStudio Server](https://hub.docker.com/r/rocker/rstudio/)
2. [Orange HR Management Solution](https://hub.docker.com/r/bitnami/orangehrm/)
3. [PION Video Conference App](https://github.com/pion/ion) on [DigitalOcean](https://www.digitalocean.com/community/questions/self-hosted-video-conferencing-options-pros-and-cons-setup-guides)
4. [Esquisse Dashboard Shiny Server](https://github.com/dreamRs/esquisse/issues/21)