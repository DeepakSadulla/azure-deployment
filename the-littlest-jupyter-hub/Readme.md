# The Littlest Jupyter Hub

A simple JupyterHub distribution for a small (0-100) number of users on a single server. We recommend reading [When to use The Littlest JupyterHub](http://tljh.jupyter.org/en/latest/topic/whentouse.html#topic-whentouse) to determine if this is the right tool for you.

## How to install on Azure
All the necessary steps to install a remote server on Azure (for a small group) are available [here](http://tljh.jupyter.org/en/latest/install/azure.html).

We use the following cloud initialization to install the server:
``` bash
#!/bin/bash
curl https://raw.githubusercontent.com/jupyterhub/the-littlest-jupyterhub/master/bootstrap/bootstrap.py \
  | sudo python3 - \
    --admin <admin-user-name>
```
Please supply the admin name here, the password can be setup during the first time login..


## For other cloud providers
If you wish to install TLJH using other cloud providers, read more [here](http://tljh.jupyter.org/en/latest/#installation).


## Administration
1. Access JupyterHub using the IP address of the machine; No need to mention the port because it is using the default port for HTTP (80) and HTTPS (443)
2. Use the user_name from the Cloud Init Step and supply a desired password to be set on the first login
3. Go to Control Panel and then to the Admin Portal
4. Add your team members and let them know the usernames created; when they login for the first time, they can set their password as well
5. Remove or elevate users as per your team's needs
6. You can stop the server to resize it (depending on your team's needs) on Azure Portal and start it back up

## Install a few data science related python packages
Upload the requirements.txt file to the JupyterHub Profile and run the following command in Jupyter's terminal as an admin:

`sudo -E pip install -r requirements.txt`